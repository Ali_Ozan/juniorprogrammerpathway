﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour
{
    public MeshRenderer Renderer;
    Material material;
    public float orbitRadius = 3.5f;
    public float wavingMotionAmplitude = 1.5f;
    public float colorChangePeriod = 2f;

    float randomRed;
    float randomGreen;
    float randomBlue;
    float randomAlpha;

    float randomScale;

    void Start()
    {
        transform.localScale = Vector3.one * 1.3f;

        material = Renderer.material;

        material.color = new Color(0.5f, 1.0f, 0.3f, 0.4f);
        RandomColorChange();
        RandomScaleChange();
    }

    private void RandomScaleChange()
    {
        randomScale = UnityEngine.Random.Range(0.8f, 1.5f);

        Invoke("RandomScaleChange", colorChangePeriod);
    }

    private void RandomColorChange()
    {
        randomRed = UnityEngine.Random.Range(0f, 1f);
        randomGreen = UnityEngine.Random.Range(0f, 1f);
        randomBlue = UnityEngine.Random.Range(0f, 1f);
        randomAlpha = UnityEngine.Random.Range(0f, 1f);
        
        Invoke("RandomColorChange", colorChangePeriod);
    }

    void Update()
    {
        //move the object around a circular path while also making it do an up and down motion
        transform.position = new Vector3(orbitRadius * Mathf.Cos(Time.time),
                                         wavingMotionAmplitude * Mathf.Sin(3 * Time.time),
                                         orbitRadius * Mathf.Sin(Time.time));
        
        //rotating the object around its 3 axes with different rates
        transform.Rotate(20.0f * Time.deltaTime, 40.0f * Time.deltaTime, 60.0f * Time.deltaTime);

        //changing the color of the object gradually to a randomly set new color.
        material.color = new Color(Mathf.Lerp(material.color.r, randomRed, 0.01f),
                                   Mathf.Lerp(material.color.g, randomGreen, 0.01f),
                                   Mathf.Lerp(material.color.b, randomBlue, 0.01f),
                                   Mathf.Lerp(material.color.a, randomAlpha, 0.01f));

        transform.localScale = Vector3.one * Mathf.Lerp(transform.localScale.x, randomScale, 0.005f);
    }
}
