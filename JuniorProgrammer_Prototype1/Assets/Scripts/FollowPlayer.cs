using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public GameObject player;
    public Vector3 cameraOffset = new Vector3(0f, 5f, -8f);

    void Start()
    {
    
    }

    void LateUpdate()
    {
        //follow the player with the specified offset
        transform.position = player.transform.position + cameraOffset;        
    }
}
