using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spinner : MonoBehaviour
{
   [SerializeField] float anglesPerSecond;

    void FixedUpdate()
    {
        transform.Rotate(Vector3.forward, Time.deltaTime * anglesPerSecond);
    }
}
