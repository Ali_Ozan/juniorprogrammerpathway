using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] float speed = 10f;
    [SerializeField] float xRange = 12f;
    public GameObject projectilePrefab;
    float horizontalInput;

    void Start()
    {
        
    }

    void Update()
    {
        HandleHorizontalMovement();
        ShootProjectiles();
    }

    private void ShootProjectiles()
    {
        if (Input.GetButtonDown("Jump"))
            Instantiate(projectilePrefab, new Vector3(transform.position.x, transform.position.y +1, transform.position.z), transform.rotation);
    }

    private void HandleHorizontalMovement()
    {

        horizontalInput = Input.GetAxis("Horizontal");
        transform.Translate(Vector3.right * horizontalInput * Time.deltaTime * speed);
        if (Mathf.Abs(transform.position.x) > xRange)
            transform.position = new Vector3((transform.position.x / Mathf.Abs(transform.position.x)) * xRange, transform.position.y, transform.position.z);
    }
}
