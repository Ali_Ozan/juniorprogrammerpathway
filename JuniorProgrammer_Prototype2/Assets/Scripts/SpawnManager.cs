using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public GameObject[] animalPrefabs;

    private void Start()
    {
        InvokeRepeating("RandomSpawn", 2f, 1.5f);
    }

    private void RandomSpawn()
    {
        Vector3 randomSpawnPosition = new Vector3(Random.Range(-17f, 17f), 0f, Random.Range(22f, 30f));
        int randomAnimalIndex = Random.Range(0, animalPrefabs.Length);
        Instantiate(animalPrefabs[randomAnimalIndex], randomSpawnPosition, animalPrefabs[randomAnimalIndex].transform.rotation);
    }
}
