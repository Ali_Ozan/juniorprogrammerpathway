using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOutOfBounds : MonoBehaviour
{
    [SerializeField] float upperBoundDistance = 50f;
    [SerializeField] float lowerBoundDistance = -10f;

    void Update()
    {
        if (transform.position.z > upperBoundDistance)
            Destroy(gameObject);
        else if (transform.position.z < lowerBoundDistance)
            Debug.Log("game over");
    }
}
