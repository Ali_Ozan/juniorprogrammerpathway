using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveForward : MonoBehaviour
{
    [SerializeField] float randomSpeedLowerBound = 5f;
    [SerializeField] float randomSpeedUpperBound = 30f;
    private float speed;

    void Start()
    {
        speed = Random.Range(randomSpeedLowerBound, randomSpeedUpperBound);
    }

    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * speed);    
    }
}
